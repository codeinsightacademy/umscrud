<?php

function add($x = 0, $y = 0) {
	return $x + $y;
}


function getConn() {

	//if($_SERVER['HTTP_HOST'] == 'localhost') {	
	if(preg_match('/^localhost/i', $_SERVER['HTTP_HOST'])) {	
		$db = parse_ini_file('/var/www/html/ums/config.ini', true);
	} else {
		$db = parse_ini_file('/home/vol18_1/epizy.com/epiz_28863810/htdocs/config.ini', true);
	}

	if ($db['env'] == "prod") {
		$servername = $db['PRD_DB_HOST'];
		$username = $db['PRD_DB_USER'];
		$password = $db['PRD_DB_PASS'];
		$dbname = $db['PRD_DB_NAME'];
	} else {
		$servername = $db['DB_HOST'];
                $username = $db['DB_USER'];
                $password = $db['DB_PASS'];
                $dbname = $db['DB_NAME'];
	}

	$conn = null;

	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}

	return $conn;

}

